import firebase from 'firebase'

var config = {
  apiKey: 'AIzaSyBWH2gtHdGlIYNaq0f1b7leFo7_eQv7h54',
  authDomain: 'cropchat-f0e15.firebaseapp.com',
  databaseURL: 'https://cropchat-f0e15.firebaseio.com',
  projectId: 'cropchat-f0e15',
  storageBucket: 'cropchat-f0e15.appspot.com',
  messagingSenderId: '221311705985'
}
firebase.initializeApp(config)

export default {
  database: firebase.database()
}
